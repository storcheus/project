<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::get('profile', 'UserController@getAuthenticatedUser');


Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('/product/relations', 'Api\ProductController@relations');
    Route::get('/product/categories', 'Api\ProductController@categories');
    Route::get('/product/expensive', 'Api\ProductController@expensive');

    Route::get('/products', 'Api\ProductController@index');
    Route::get('/categories', 'Api\CategoryController@index');

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

