@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

                    <div class="col-md-4">
                        <div class="card-header">Block0 (relations)</div>

                        <div class="card-body">
                            <table border="2">
                                <thead>
                                <tr>
                                    <th> product id</th>
                                    <th> product title</th>
                                    <th> product price</th>
                                    <th> category id</th>
                                    <th> category title</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($relations as $relation)
                                        <tr>
                                            <td> {{$relation->product_id}} </td>
                                            <td> {{$relation->product_title}} </td>
                                            <td> {{$relation->product_price}} </td>
                                            <td> {{$relation->category_id}}  </td>
                                            <td> {{$relation->category_title}} </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

            <div class="col-md-5">

            <div class="card-header">Block1</div>

                    <div class="card-body">
                        <table border="2">
                            <thead>
                            <tr>
                                <th> id</th>
                                <th> title</th>
                                <th> count categories</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td> {{$product->id}} </td>
                                        <td> {{$product->title}} </td>
                                        <td> {{$product->count_categories}} ({{$product->categories}}) </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-header">Block2</div>

                    <div class="card-body">
                        @for ($i = 0; $i < count($expensive); $i+=2)
                            <p>  {{ $expensive[$i]->title }} + {{ $expensive[$i+1]->title }} = {{  $expensive[$i]->price +  $expensive[$i+1]->price }}</p>
                        @endfor
                    </div>
                </div>
        </div>
    </div>
@endsection


