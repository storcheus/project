<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DatabaseCreateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'db:create';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new database schema based on the .env file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $driver = env('DB_CONNECTION');
        $schemaName = env('DB_DATABASE');

        config(["database.connections.".$driver.".database" => null]);

        switch ($driver) {
             case 'sqlite':
                 //
                 break;

             case 'pgsql':
                 $query = "CREATE DATABASE $schemaName";
                 break;

             case 'sqlsrv':
                //
                 break;

                 //mysql
             default:
                 $query = "CREATE DATABASE IF NOT EXISTS $schemaName";
                 break;
         }

        try {
            DB::statement($query);
            $this->info(sprintf('Successfully created %s database', $schemaName));
        } catch (\Exception $exception) {
            $this->error(sprintf('Failed to create %s database, %s', $schemaName, $exception->getMessage()));
        }

        config(["database.connections.".$driver.".database" => $schemaName]);

    }
}
