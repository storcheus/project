<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('product.index', [
            'products' => $this->categories(),
            'expensive' => $this->expensive(),
            'relations' => $this->relations(),
        ]);
    }

    public function expensive()
    {
        $pairs = DB::table('products')
            ->orderByDesc('price')
            ->limit(6)
            ->get();

        return $pairs;
    }

    public function categories()
    {
        $records =  DB::table('product_category')
            ->join('products', 'product_category.product_id', '=', 'products.id')
            ->join('categories', 'product_category.category_id', '=', 'categories.id')
            ->select(   DB::raw('count(categories.id) as count_categories, (GROUP_CONCAT(categories.title SEPARATOR ", ")) as `categories`, products.id, products.title'))
            ->groupBy('products.id')
            ->orderByDesc('count_categories')
            ->having('count_categories', '>', 1)
            ->get();

        return $records;
    }

    public function relations()
    {
        $records =  DB::table('product_category')
            ->join('products', 'product_category.product_id', '=', 'products.id')
            ->join('categories', 'product_category.category_id', '=', 'categories.id')
            ->select(   'products.id as product_id', 'products.title as product_title', 'products.price as product_price', 'product_category.category_id as category_id', 'categories.title as category_title')
            ->orderBy('products.id')
            ->get();

        return $records;
    }
}