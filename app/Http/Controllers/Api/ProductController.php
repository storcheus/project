<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function index()
    {
        $products = DB::table('products')->get();

        return response()->json($products);
    }

    public function expensive()
    {
        $records = DB::table('products')
            ->orderByDesc('price')
            ->limit(6)
            ->get();

        $rows = [];
        $j=0;
        for ($i = 0; $i < count($records); $i+=2) {
            $rows[$j]['title1']  = $records[$i]->title;
            $rows[$j]['title2']  = $records[$i+1]->title;
            $rows[$j++]['total']  = $records[$i]->price +  $records[$i+1]->price;
        }

        return response()->json($rows);
    }

    public function categories()
    {

        $records =  DB::table('product_category')
            ->join('products', 'product_category.product_id', '=', 'products.id')
            ->join('categories', 'product_category.category_id', '=', 'categories.id')
            ->select(   DB::raw('count(categories.id) as count_categories, (GROUP_CONCAT(categories.title SEPARATOR ", ")) as `categories`, products.id, products.title'))
            ->groupBy('products.id')
            ->orderByDesc('count_categories')
            ->having('count_categories', '>', 1)
            ->get();

        return response()->json($records);
    }

    public function relations()
    {

        $records =  DB::table('product_category')
            ->join('products', 'product_category.product_id', '=', 'products.id')
            ->join('categories', 'product_category.category_id', '=', 'categories.id')
            ->select(   'products.id as product_id', 'products.title as product_title', 'products.price as product_price', 'product_category.category_id as category_id', 'categories.title as category_title')
            ->orderBy('products.id')
            ->get();

        return response()->json($records);
    }
}