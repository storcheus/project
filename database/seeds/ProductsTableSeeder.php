<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 10; $i++) {
            DB::table('products')->insert([
                'title' => 'product' . $i,
                'price' => rand(10,1000),
            ]);
        }
    }
}