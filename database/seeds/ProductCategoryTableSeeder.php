<?php

use Illuminate\Database\Seeder;

class ProductCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $rows = [
            [
                'category_id' => '1',
                'product_id' => '7',
            ],
            [
                'category_id' => '1',
                'product_id' => '2',
            ],
            [
                'category_id' => '1',
                'product_id' => '3',
            ],
            [
                'category_id' => '2',
                'product_id' => '3',
            ],
            [
                'category_id' => '2',
                'product_id' => '4',
            ],
            [
                'category_id' => '2',
                'product_id' => '5',
            ],
            [
                'category_id' => '3',
                'product_id' => '3',
            ],
            [
                'category_id' => '4',
                'product_id' => '5',
            ],
            [
                'category_id' => '4',
                'product_id' => '6',
            ],
            [
                'category_id' => '4',
                'product_id' => '7',
            ],
            [
                'category_id' => '4',
                'product_id' => '8',
            ],
            [
                'category_id' => '5',
                'product_id' => '8',
            ],
            [
                'category_id' => '5',
                'product_id' => '9',
            ],
            [
                'category_id' => '6',
                'product_id' => '10',
            ],
        ];

        for ($i = 0; $i < count($rows); $i++) {
            DB::table('product_category')->insert([
                'category_id' => $rows[$i]['category_id'],
                'product_id'  => $rows[$i]['product_id'],
            ]);
        }
    }
}